# Cintelink Test

## Instalar dependencias
Utilizar node v16

```
yarn install
```

### Servir el sitio
```
yarn serve
```

*Si el servidor da error de CORS (en la consola del chrome), en el navegador Chrome se puede instalar la extension [Allow CORS: Access-Control-Allow-Origin](https://chrome.google.com/webstore/detail/allow-cors-access-control/lhobafahddgcelffkeicbaginigeejlf?hl=es) para validar la petición 

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
